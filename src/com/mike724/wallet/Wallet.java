package com.mike724.wallet;

import java.io.File;
import java.sql.SQLException;
import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.SQLite;

import org.bukkit.plugin.java.JavaPlugin;

public class Wallet extends JavaPlugin {
	
	@Override
	public void onEnable() {
		Logger log = this.getLogger();
		File dataFolder = this.getDataFolder();
		if(!dataFolder.exists()) {
			dataFolder.mkdir();
		}
		log.info("Initializing SQLite");
		Storage.db = new SQLite(log, "SQLite", "Players", dataFolder.getPath());
		try {
			Storage.db.open();
		} catch (SQLException e) {
			log.severe("Could not open SQLite database! Disabling");
			e.printStackTrace();
			this.getServer().getPluginManager().disablePlugin(this);
		}
		if (!Storage.db.checkTable("players")) {
			log.info("Creating table players");
			String query = "CREATE TABLE `players` (`id` INT NOT NULL AUTO_INCREMENT DEFAULT 0,`name` VARCHAR NOT NULL DEFAULT 'Player',`money` DECIMAL NOT NULL DEFAULT 0,PRIMARY KEY (`id`));";
			Storage.db.createTable(query);
		}
		
	}

	@Override
	public void onDisable() {
		// TODO Auto-generated method stub
		super.onDisable();
	}

}
